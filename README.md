# Exercicio2_IncubadoraDBServer

## Revisão 

### Exercícios 

**5) Desenvolva código para criar, atualizar e mostrar detalhes (create, update, details) de PetType e Vet Specialty:**

- [x] 5a) Utilize sua PetClinic no Gitlab (git, STS). ✔
- [x] 5b) Registre e gerencie ordens de serviço (issues, GitLab) necessárias. ✔
- [x] 5c) Adicione os controladores (controller, Spring), rotas GET e POST necessárias. ✔
- [x] 5d) Mova as listas para um atributo nos controladores para simular dados necessários e manutenção de estados entre métodos: ✔

**Por exemplo:** 

```@Controller 
public class PetTypeController {
    static ArrayList<PetType> pets = new ArrayList<PetType>();
    static final String[ ] petNames = {"cat", "dog", "lizard", "snake", "bird", "hamster"}; 
    static {
        for (String n : petNames) { 
            PetType p = new PetType(); 
            p.setName(n); 
            pets.add(p); 
            } 
        }                    
    // restante do controlador
 } 
```

**O controlador deve utilizar o atributo petNames que pertence à classe. Esta é uma "gambiarra" temporária, apenas para podermos trabalhar com MVC sem o uso de classes persistência.**

- [x] 5e) Adicione as vistas (view, Thymeleaf) necessárias (createOrUpdate, Details). ✔
- [ ] 5f) [EXTRA] Adicione testes para o código desenvolvido. **(PENDENTE)**
- [x] 5g) [EXTRA] Adicione a navegação entre páginas com botões e referências para completar a tela do usuário. ✔

_Utilize o material da Spring sobre "forms" e o código da PetClinic como referência._ 

- [x] 6) [EXTRA] Desenvolva código para completar as mesmas funcionalidade para Product: ✔
- [ ] 7) [EXTRA] Altere a cor padrão da aplicação para outra cor de sua preferência. **(PENDENTE)**


